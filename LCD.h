﻿/*
 * LCD.h
 *
 * Created: 2017. 10. 21. 12:15:46
 *  Author: Ádám
 */ 


#ifndef LCD_H_
#define LCD_H_

extern void lcd_init (void);
extern void lcd_cmd (char parancs);
extern void lcd_data(char adat);
extern void lcd_out(uint8_t *s);
extern void LCD_goto(unsigned char, unsigned char);	




#endif /* LCD_H_ */
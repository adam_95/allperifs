﻿/*
 * initek.h
 *
 * Created: 2017. 10. 24. 14:07:08
 *  Author: Ádám
 */ 


#ifndef INITEK_H_
#define INITEK_H_

#include <avr/io.h>
#include <avr/interrupt.h>

#define F_CPU 8000000UL


extern void TIMER_init(void);
extern void LED_init(void);



#endif /* INITEK_H_ */
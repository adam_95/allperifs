﻿/*
 * periferiak.c
 *
 * Created: 2017. 10. 24. 14:42:33
 *  Author: Ádám
 */ 

#include "periferiak.h"

void lcd_re(int8_t ertek)
{
		switch(ertek)
		{
		case 1 : {lcd_data('1'); break;};
		case 2 : {lcd_data('2'); break;};
		case 3 : {lcd_data('3'); break;};
		case 4 : {lcd_data('4'); break;};
		case 5 : {lcd_data('5'); break;};
		case 6 : {lcd_data('6'); break;};
		case 7 : {lcd_data('7'); break;};
		case 8 : {lcd_data('8'); break;};
		case 9 : {lcd_data('9'); break;};
		case 0 : {lcd_data('0'); break;};
		}
}



uint16_t ossz(int8_t *sz)
{
	uint32_t sum = 0;
	uint8_t szamolo = 0;
	while(*sz != (-2)) szamolo++;
	switch (szamolo)
	{
		case 1: {sum = sz[0]; break;}
		case 2: {sum = sz[0]*10+sz[1]; break;}
		case 3: {sum = sz[0]*100+sz[1]*10+sz[2]; break;}
		case 4: {sum = sz[0]*1000+sz[1]*100+sz[2]*10+sz[3]; break;}
		case 5: {sum = sz[0]*10000+sz[1]*1000+sz[2]*100+sz[3]*10+sz[4]; break;}
		case 6: {sum = sz[0]*100000+sz[1]*10000+sz[2]*1000+sz[3]*100+sz[4]*10+sz[5]; break;}
	}
return sum;
}


void LED_out(uint16_t led)
{
	PORTB = led << 4;
	PORTD = led;
}
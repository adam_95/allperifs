/*
 * 2zh_s.c
 *
 * Created: 2017. 10. 24. 13:22:24
 * Author : Ádám
 */ 

#include <avr/io.h>

#include "bill.h"
#include "LCD.h"
#include "menu_ki.h"
#include "initek.h"
#include "periferiak.h"


volatile int8_t kgb_value = (-1);
volatile int8_t szamok[6] = {-2};
volatile uint8_t hanyadik = 0,figyelo = 0, tmb_szam = 0;


int main(void)
{
	
	uint8_t torlo = 0, *pt, i;
	pt = &torlo;
	
	kgb_init();
	lcd_init();
	TIMER_init();
	LED_init();
	
	lcd_cmd(0x01);
	lcd_cmd(0x80);
	lcd_cmd(0x06);
	
	
	uint8_t num[15] = {"num:"};								
	uint8_t menu[15] = {"menu"};
	uint8_t adc[15] = {"ADC:"};
	uint8_t rgb[15] = {"RGB:"};
		
	uint8_t G0[15] = {"G0:karakt. tor."};
	uint8_t G1[15] = {"G1:be, szam"};
	uint8_t G2[15] = {"G2:mp, G3:perc"};
	uint8_t G3[15] = {"G4:minden torol"};
		
	
			
		
		
	
    while (1) 
    {
		if(hanyadik == 6) hanyadik = 0; 
		if(kgb_value == 100)	
		{
			i=6;
			while(i--) szamok[i] = *pt;
			lcd_cmd(0x01);
			lcd_menu(G0,G1,G2,G3);
			hanyadik = 0;
			figyelo = 0;
		}
		if (kgb_value == 101)
		{
			i=6;						
			while(i--) szamok[i] = *pt;		//"kinullázom" a tömböt azért -2 vwl mert ezt az értéket nem veszi figyelembe a program
			lcd_cmd(0x01);
			lcd_menu(num,menu,adc,rgb);
			hanyadik = 0;
			figyelo = 0;
		}
    }
}


ISR(TIMER1_COMPA_vect)
{
	kgb_value = kgb_in();
	if (kgb_value != (-1) && figyelo < 6)
		{
			LCD_goto(0,5+hanyadik);
			szamok[tmb_szam] = kgb_value;
			tmb_szam++;
			hanyadik++;
			figyelo++;
			lcd_re(kgb_value);			
		}
}

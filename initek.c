﻿/*
 * initek.c
 *
 * Created: 2017. 10. 24. 14:06:54
 *  Author: Ádám
 */ 


#include "initek.h"


void TIMER_init(void)
{
	TCCR1B |= (1 << WGM12);
	TCCR1B |= (1 << CS10)|(1 << CS12);
	TCNT1L = 0x00;
	TCNT1H = 0x00;
	OCR1AH = 0x01;
	OCR1AL = 0x86;
	TIMSK |= (1 << OCIE1A);
	sei();
}


void LED_init(void)
{
	DDRD = 0xF0;
	DDRB = 0xF0;
}
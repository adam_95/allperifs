﻿/*
 * bill.c
 *
 * Created: 2017. 10. 21. 12:42:54
 *  Author: Ádám
 
 a bill_sort deklarálni kell a mainben, meghívni egy sima függvényhívás és működik az egész
 tehát a használatához a következő kell
 
 #include "bill.h"
 kgb_init();
 unsigned char kgb_value = kgb_in();
 
 */ 

#include "bill.h"

void kgb_init(void)
{
	DDRC = 0xF8;
}

unsigned char bill_sor = 4;

int8_t kgb_in()
{
	unsigned char keyb, i = 0, cimzo;
	int8_t szam = (-1);
	unsigned char kgb_tmb[24] = {69,0, 14,1, 13,2, 11,3, 22,4, 21,5, 19,6, 38,7, 37,8, 35,9, 67,101,70,100};
	if(bill_sor == 1)	cimzo = 0x08;
	if(bill_sor == 2)	cimzo = 0x10;
	if(bill_sor == 3)	cimzo = 0x20;
	if(bill_sor == 4)	{cimzo = 0x40; bill_sor = 0;}
	bill_sor++;
	PORTC = cimzo;
	keyb = PINC;
	while(i < 24)
	{
		if(keyb == kgb_tmb[i])
		{
			szam = kgb_tmb[i+1];
			i+=2;
		}
		i++;
	}
	if(szam != (-1))
	return szam;
	else
	{
		return -1;
	}
}

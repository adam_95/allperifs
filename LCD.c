﻿/*
 * LCD.c
 *
 * Created: 2017. 10. 21. 12:13:50
 *  Author: Ádám
 
 az initeket még így is meg kell hívni amúgy jól működik
 
 //itoa -> int to string	//(mit, hova, számrendszer)			HASZNOS 
 */ 

#include <avr/io.h>
#define  F_CPU  8000000UL
#include <util/delay.h>

void lcd_init (void)
{
	DDRF = 0b00001110;
	DDRE = 0b11110000;

	_delay_ms(500);

	
	lcd_cmd(0b00110000);

	lcd_cmd(0x02);
	lcd_cmd(0x01);

	lcd_cmd(0x0C);
	

}

void lcd_cmd (char parancs)
{
	PORTF = PORTF & 0b11111101;

	PORTE = parancs & 0xF0;
	_delay_ms(1);
	PORTF= PORTF | 0b00001000;
	_delay_ms(1);
	PORTF= PORTF & 0b11110111;
	
	PORTE = (parancs<<4) &0xF0;
	_delay_ms(1);
	PORTF= PORTF | 0b00001000;
	_delay_ms(1);
	PORTF= PORTF & 0b11110111;
}

void lcd_data(char adat)
{
	PORTF = PORTF | 0b00000010;

	PORTE = adat & 0xF0;
	_delay_ms(1);
	PORTF= PORTF | 0b00001000;
	_delay_ms(1);
	PORTF= PORTF & 0b11110111;

	PORTE = (adat<<4) &0xF0;
	_delay_ms(1);
	PORTF= PORTF | 0b00001000;
	_delay_ms(1);
	PORTF= PORTF & 0b11110111;
}



void lcd_out(uint8_t *s)
{
 while (*s)
 {
	 lcd_data(*s);
	 *s++; 
 }
}



void LCD_goto(unsigned char row, unsigned char col)		//sor,oszlop
{
	if((col>=16) || (row>=4))return;
	if(row>=2)
	{
		row = row - 2;
		col = col + 16;
	}
	lcd_cmd((1<<7)|(row<<6)|col);
}
